﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Exemple.Layout.Presentation.App.Converters
{
    public class PercentageToSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null)
                return value;

            return System.Convert.ToDouble(value) * (System.Convert.ToDouble(parameter) / 100);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
