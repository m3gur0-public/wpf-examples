﻿using System.Windows;

namespace Exemple.Layout.Presentation.App
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void GridButton_Click(object sender, RoutedEventArgs e)
        {
            Grid gridWindow = new Grid();
            gridWindow.Show();
        }
        
        private void StackButton_Click(object sender, RoutedEventArgs e)
        {
            StackPanel stackWindow = new StackPanel();
            stackWindow.Show();
        }
        
        private void CanvasButton_Click(object sender, RoutedEventArgs e)
        {
            Canvas canvasWindow = new Canvas();
            canvasWindow.Show();
        }

        private void DockPanelButton_Click(object sender, RoutedEventArgs e)
        {
            DockPanel dockPanelWindow = new DockPanel();
            dockPanelWindow.Show();
        }
    }
}
