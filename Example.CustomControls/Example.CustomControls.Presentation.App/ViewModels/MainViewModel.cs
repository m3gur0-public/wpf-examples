﻿using Example.CustomControls.Presentation.App.Commands;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Example.CustomControls.Presentation.App.ViewModels
{
    class MainViewModel : INotifyPropertyChanged
    {
        private static readonly string baseImageUri = "pack://application:,,,/Example.CustomControls.Presentation.ControlLibrary;component/Resources/Images/pilon/";

        private string imageSource;
        private bool isAnimated = false;
        private bool isRotated = false;
        private short minPositionY = -30;
        private short maxPositionY = 30;
        private short positionY;
        private short angle = 0;
        private BitmapImage mixingImage;

        public ICommand AnimationCommand { get; set; }

        public ICommand RotationCommand { get; set; }

        public bool IsAnimated
        {
            get { return isAnimated; }
            set
            {
                isAnimated = value;
                OnPropertyChanged(nameof(IsAnimated));
            }
        }

        public bool IsRotated
        {
            get { return isRotated; }
            set
            {
                isRotated = value;
                OnPropertyChanged(nameof(IsRotated));
            }
        }

        public short Angle
        {
            get { return angle; }
            set
            {
                angle = value;
                OnPropertyChanged(nameof(Angle));
            }
        }

        public short MinPositionY
        {
            get { return minPositionY; }
            set
            {
                minPositionY = value;
                OnPropertyChanged(nameof(MinPositionY));
            }
        }

        public short MaxPositionY
        {
            get { return maxPositionY; }
            set
            {
                maxPositionY = value;
                OnPropertyChanged(nameof(MaxPositionY));
            }
        }

        public short PositionY
        {
            get { return positionY; }
            set
            {
                positionY = value;
                OnPropertyChanged(nameof(PositionY));
            }
        }

        public BitmapImage MixingImage
        {
            get { return mixingImage; }
            set
            {
                mixingImage = value;
                OnPropertyChanged(nameof(MixingImage));
            }
        }

        public string ImageSource
        {
            get { return imageSource; }
            set
            {
                imageSource = value;
                OnPropertyChanged(nameof(ImageSource));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public MainViewModel()
        {
            positionY = minPositionY;
            imageSource = $"{baseImageUri}Melange01.png";
            mixingImage = new BitmapImage(new Uri(ImageSource));

            AnimationCommand = new RelayCommand(async x =>
            {
                short loopDirection = 1;

                while (IsAnimated)
                {
                    await Animate(loopDirection);
                    loopDirection = (short)(loopDirection * -1);
                }
            });

            RotationCommand = new RelayCommand(async x =>
            {
                while (IsRotated) await Rotate();
            });
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async Task Animate(short loopDirection)
        {
            for (short i = minPositionY; i <= maxPositionY; i++)
            {
                PositionY = (short)(i * loopDirection);
                await Task.Delay(10);
            }
        }

        private async Task Rotate()
        {
            int imageIndex = 5;

            for (short i = 0; i <= 360; i++)
            {
                if (i % 72 == 0 && i != 0) imageIndex = i / 72;
                ImageSource = $"{baseImageUri}Melange0{imageIndex}.png";
                MixingImage = new BitmapImage(new Uri(ImageSource));
                
                Angle = i;
                await Task.Delay(1);
            }
        }
    }
}
