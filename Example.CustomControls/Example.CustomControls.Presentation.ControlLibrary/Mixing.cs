﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Example.CustomControls.Presentation.ControlLibrary
{
    public class Mixing : Control
    {
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(Mixing), new FrameworkPropertyMetadata(0.0));

        public static readonly DependencyProperty PositionYProperty =
            DependencyProperty.Register("PositionY", typeof(double), typeof(Mixing), new FrameworkPropertyMetadata(0.0));

        public static readonly DependencyProperty ImageProperty =
                    DependencyProperty.Register("Image", typeof(ImageSource), typeof(Mixing), new UIPropertyMetadata(null));

        static Mixing()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Mixing), new FrameworkPropertyMetadata(typeof(Mixing)));
        }

        public ImageSource Image
        {
            get { return (ImageSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        public double PositionY
        {
            get { return (double)GetValue(PositionYProperty); }
            set { SetValue(PositionYProperty, value); }
        }

        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }
    }
}
