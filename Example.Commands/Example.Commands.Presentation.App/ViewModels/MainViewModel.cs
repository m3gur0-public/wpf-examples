﻿using Example.Commands.Presentation.App.Command;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace Example.Commands.Presentation.App.ViewModels
{
    class MainViewModel : INotifyPropertyChanged
    {
        private Brush color = Brushes.Gray;

        public event PropertyChangedEventHandler PropertyChanged;

        public Brush Color
        {
            get { return color; }
            set
            {
                color = value;
                OnPropertyChanged(nameof(Color));
            }
        }

        public ICommand DefaultCommand { get; set; }

        public MainViewModel()
        {
            DefaultCommand = new RelayCommand(async x =>
            {
                Color = Brushes.Green;
                await Task.Delay(500);
                Color = Brushes.Red;
                await Task.Delay(500);
                Color = Brushes.Violet;
                await Task.Delay(500);
                Color = Brushes.Blue;
                await Task.Delay(500);
                Color = Brushes.Gray;
            });
        }

        private void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
