﻿using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;
using Example.Navigation.Presentation.App.Commands;

namespace Example.Navigation.Presentation.App.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private object selectedViewModel;

        public event PropertyChangedEventHandler PropertyChanged;
        
        public ICommand OpenView1Command { get; set; }

        public ICommand OpenView2Command { get; set; }

        public ICommand OpenView3Command { get; set; }

        public object SelectedViewModel
        {
            get { return selectedViewModel; }
            set
            {
                selectedViewModel = value;
                OnPropertyChanged(nameof(SelectedViewModel));
            }
        }

        public MainViewModel()
        {
            SelectedViewModel = new ViewModel1();
            

            OpenView1Command = new RelayCommand(v => SelectedViewModel = new ViewModel1(), 
                c => SelectedViewModel.GetType() != typeof(ViewModel1));
            OpenView2Command = new RelayCommand(v => SelectedViewModel = new ViewModel2(), 
                c => SelectedViewModel.GetType() != typeof(ViewModel2));
            OpenView3Command = new RelayCommand(v => SelectedViewModel = new ViewModel3(), 
                c => SelectedViewModel.GetType() != typeof(ViewModel3));
        }

        private void OnPropertyChanged(string propName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
    }
}
