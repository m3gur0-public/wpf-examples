﻿using Example.Navigation.Presentation.App.Commands;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace Example.Navigation.Presentation.App.ViewModels
{
    class ViewModel1 : INotifyPropertyChanged
    {
        private string text;

        public ICommand ChangeItCommand { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        public ViewModel1()
        {
            ChangeItCommand = new RelayCommand(o => { Text = DateTime.Now.ToString(); });
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
